#include <ros/ros.h>
#include <SDL2/SDL.h>
#include <arm/int16List.h>

#include <iostream>
#include <vector>
//#include "std_msgs/Int8MultiArray.h"


int rt=0;
int lt=0;
int lsx=0;
int rsx=0;
int lsy=0;
int rsy=0;

float th=0;
float r=220;
float z=120;
float alpha=8;
float gama=0;
float sig=-8;


float nth=0;
float nr=114;
float nz=58;
float nalpha=8;
float ngama=0;
float nsig=-8;

//these are the legnths of the segments of the arm, currently they are in 1/10 of inch,
//these are the only values that need to be changed for units to be changed
float L1=176.3;
float L2=161.2;
float L3=27.5;

//indicates the max angle physical angle of each joint
float a_max=90;  
float b_max=103;
float c_max=145;
float d_max=229;
float e_max=0;
float f_max=360;

//indicates the min angle physical angle of each joint
float a_min=-90;
float b_min=-4.5;
float c_min=35;
float d_min=143;
float e_min=136;
float f_min=0;

//These values are the in degrees, they are for testing valid position only, they are not passed
float a=0;
float b=0;
float c=0;
float d=0;
float e=0;
float f=0;

float pi=3.14159265358979323846264338327950;

void calcAngles(float nr,float nz, float nsig){
  //all 3 angles are confined to the plane
  //the wrist joint is found at (x2,y2), 
  // with r2 being the distance to the shoulder joint, 
  // and beta being the angle of r2
  float x2 = nr - L3*cos(nsig*pi/180);
  float y2 = nz - L3*sin(nsig*pi/180);
  float r2= sqrt(x2*x2+y2*y2);
  float beta = atan2(y2,x2);//in radians
  
  //the law of cosines gives angle c in radians, which is then converted to degrees
  c=(acos((L1*L1+L2*L2-r2*r2)/(2*L1*L2)))*180/pi;
  
  //the law of cosines gives the angle between shoulder beam and r2
  //adding this to beta gives b in radians, which can then be converted to degrees  
  b=(beta +acos((L1*L1-L2*L2+r2*r2)/(2*L1*r2)))*180/pi;
  
  //because b,c,d are confied to a plane they must add up to the target angle sigma (already in degrees)
  d=-sig+b+c;
}

int main(int argc, char **argv)
{

    ros::init(argc, argv, "piArm");
    ros::NodeHandle nh;
    ros::Publisher armPub = nh.advertise<arm::int16List>("cArm", 1);
    int rosrate =30;
    ros::Rate loop_rate(rosrate);
    
    int sdlResult = SDL_Init(SDL_INIT_GAMECONTROLLER);
    SDL_GameController *mController;
    mController = SDL_GameControllerOpen(1);

    if (!mController) 
    {
        ROS_INFO("NO CONTROLLER");
    }
    while (ros::ok())
    {
        SDL_Event event;
        while(SDL_PollEvent(&event)) {
            switch (event.type) {

            case SDL_QUIT:
                break;
            }
        }

        arm::int16List msg;
	
	rt=0;
	lt=0;
	lsx=0;
	rsx=0;
	lsy=0;
	rsy=0;

	if(SDL_GameControllerGetButton(mController, SDL_CONTROLLER_BUTTON_A)==1){
	 th=0;
	 r=220;
	 z=120;
	 alpha=8;
	 gama=0;
	 sig=-8;
 	}


        if(SDL_GameControllerGetAxis(mController, SDL_CONTROLLER_AXIS_TRIGGERRIGHT) >= 1600)
        {
	    rt = SDL_GameControllerGetAxis(mController, SDL_CONTROLLER_AXIS_TRIGGERRIGHT);
	    rt = rt -600 * ((rt > 0) - (rt < 0)); 
        }

        if(SDL_GameControllerGetAxis(mController, SDL_CONTROLLER_AXIS_TRIGGERLEFT) >= 1600)
        {
            lt = SDL_GameControllerGetAxis(mController, SDL_CONTROLLER_AXIS_TRIGGERLEFT);
	    lt = lt - 1600 * ((lt > 0) - (lt < 0)); 
        }

        if(SDL_GameControllerGetAxis(mController, SDL_CONTROLLER_AXIS_RIGHTX) >= 2600 || SDL_GameControllerGetAxis(mController, SDL_CONTROLLER_AXIS_RIGHTX) <= -2600)
        {
            rsx = SDL_GameControllerGetAxis(mController, SDL_CONTROLLER_AXIS_RIGHTX);
	    rsx = rsx - 2600 * ((rsx > 0) - (rsx < 0)); 	 
	 
        }
	   
        if(SDL_GameControllerGetAxis(mController, SDL_CONTROLLER_AXIS_RIGHTY) >= 2600 || SDL_GameControllerGetAxis(mController, SDL_CONTROLLER_AXIS_RIGHTY) <= -2600)
        {
            rsy = -SDL_GameControllerGetAxis(mController, SDL_CONTROLLER_AXIS_RIGHTY);
	    rsy = rsy - 2600 * ((rsy > 0) - (rsy < 0)); 
        }

        if(SDL_GameControllerGetAxis(mController, SDL_CONTROLLER_AXIS_LEFTX) >= 3500 || SDL_GameControllerGetAxis(mController, SDL_CONTROLLER_AXIS_LEFTX) <= -3500)
        {
            lsx = SDL_GameControllerGetAxis(mController, SDL_CONTROLLER_AXIS_LEFTX);
	    lsx = lsx - 3500  * ((lsx > 0) - (lsx < 0)); 
        }
        if(SDL_GameControllerGetAxis(mController, SDL_CONTROLLER_AXIS_LEFTY) >= 2600 || SDL_GameControllerGetAxis(mController, SDL_CONTROLLER_AXIS_LEFTY) <= -2600)
        {
            lsy = -SDL_GameControllerGetAxis(mController, SDL_CONTROLLER_AXIS_LEFTY);
            lsy = lsy - 2600 * ((lsy > 0) - (lsy < 0)); 
        }

	 a= th + lsx/10000.0f/rosrate * 4;
	
	 if (a>a_min && a<a_max){
	th=a;
	}

	e= alpha+rsx/10000.0f/rosrate*29;
	if (e<e_min && e>e_max){
        alpha=e;
        }

	f= gama+0;
	if (f>f_min && f<f_max){
	gama=f;
        }
	
	//triggers
	float xi=(rt-lt)/100000.0f/rosrate*29;
	calcAngles(r+xi*cos(sig*pi/180),z+xi*sin(sig*pi/180),sig);
	if (b>b_min && b<b_max && c>c_min && c<c_max && d>d_min && d<d_max){
	r=r+xi*cos(sig*pi/180);
  	z=z+xi*sin(sig*pi/180);
     }
 	float eta=(rsy)/20000.0f/rosrate*18;
        calcAngles(r-eta*sin(sig*pi/180),z+eta*cos(sig*pi/180),sig);
        if (b>b_min && b<b_max && c>c_min && c<c_max && d>d_min && d<d_max){
        r=r-eta*sin(sig*pi/180);
        z=z+eta*cos(sig*pi/180);
     } 
	float delsig=lsy/20000.0f/rosrate*16;
	calcAngles(r,z,sig+delsig);
        if (b>b_min && b<b_max && c>c_min && c<c_max && d>d_min && d<d_max){
        sig=sig+delsig;

     }else{

     printf("%d,%d,%d,%d,%d,%d",b>b_min, b<b_max ,c>c_min ,c<c_max , d>d_min ,d<d_max);

      }
	std::vector<int16_t> stuff (6, 0);
	stuff[0] = th;
	stuff[1] = r;
	stuff[2] = z;
	stuff[3] = sig;
	stuff[4] = alpha;
	stuff[5] = gama;
	msg.data = stuff;;

	if( r != nr || z != nz || th != nth || sig != nsig || alpha != nalpha || gama != ngama){
        ROS_INFO("[%f, %f, %f, %f, %f, %f]", a,b,c,d,e,f);


        armPub.publish(msg);

        ros::spinOnce();

	nr=r;
	nalpha=alpha;
	ngama=gama;
	nz=z;
	nsig=sig;
	nth=th;
	
	
	
	}
        loop_rate.sleep();
 
   }
    ROS_INFO("IM NOT OKAY");
    return 0;
}
