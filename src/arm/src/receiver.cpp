#include <ros/ros.h>
#include <arm/int16List.h>
#include <stdio.h>
#include <string>
//#include "std_msgs/Int8MultiArray.h"
#include <iostream>

void chatterCallback(const arm::int16List::ConstPtr& msg)
{

    ROS_INFO("th:%d z:%d r:%d sig:%d alpha:%d gamma:%d]", msg->data[0], msg->data[1], msg->data[2], msg->data[3], msg->data[4], msg->data[5]);    
}


int main(int argc, char **argv)
{	
    printf("hello\n"); 
	
    ros::init(argc, argv, "listener");

    ros::NodeHandle n;

    ros::Subscriber sub = n.subscribe("cArm", 1, chatterCallback);

    ros::spin();

    return 0;

}
