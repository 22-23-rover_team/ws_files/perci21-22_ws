/*
 * The receiver node for the arms joint control.
 *
 * Takes in the information from the arm_relay.py
 */

#if (ARDUINO >= 100)
 #include <Arduino.h>
#else
 #include <WProgram.h>
#endif

#include <ros.h>
#include <std_msgs/Int16MultiArray.h>


// Shoulder Joint
int b1 = 2;
int b2 = 3;

// Elbow joint
int s1 = 7;
int s2 = 8;

// Wrist Joint
int w1 = 11;
int w2 = 12;

ros::NodeHandle  nh;

void forward(int pin1, int pin2)
{
  digitalWrite(pin1, LOW);
  digitalWrite(pin2, HIGH);
}


void backward(int pin1, int pin2)
{
  digitalWrite(pin1, HIGH);
  digitalWrite(pin2, LOW);
}

void stop(int pin1, int pin2)
{
  digitalWrite(pin1, LOW);
  digitalWrite(pin2, LOW);
}

void check_value(int check, int pin1, int pin2)
{
  if (check > 1)
  {
    forward(pin1, pin2);
  }
  else if (check < -1)
  {
    backward(pin1, pin2);
  }
  else{
    stop(pin1, pin2);
  }
 
}

void relay_cb( const std_msgs::Int16MultiArray& cmd_msg){
  int base = cmd_msg.data[0];
  int shoulder = cmd_msg.data[1];
  int wrist = cmd_msg.data[2];
  check_value(base, b1, b2);
  check_value(shoulder, s1, s2);
  check_value(wrist, w1, w2);
  delay(20);
  stop(b1, b2);
  stop(s1, s2);
  stop(w1, w2);
}


ros::Subscriber<std_msgs::Int16MultiArray> sub("arm_joints", relay_cb);

void setup(){
  pinMode(b1, OUTPUT);
  pinMode(b2, OUTPUT);
  pinMode(s1, OUTPUT);
  pinMode(s2, OUTPUT);
  pinMode(w1, OUTPUT);
  pinMode(w2, OUTPUT);
  nh.initNode();
  nh.subscribe(sub);
}

void loop(){
  nh.spinOnce();
  delay(1);
}
